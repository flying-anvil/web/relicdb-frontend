// @flow

import type {Gemstone} from "../Types/Gemstone";
import {builtinGemstones} from "../Data/BuiltinDb/Gemstones";
import type {Tag} from "../Types/Tag";
import {sleep} from "./Sleeper";

const host = process.env.REACT_APP_API_HOST;

// ####### Reading Endpoints ###################################################

export type StatusApiResponse = {
  status: number,
  health: 'pending' | 'healthy' | 'down',
}

export type GemApiResponse = {
  status: number,
  gemstones: Array<Gemstone>,
}

export type TagsApiResponse = {
  status: number,
  tags: Array<Tag>,
}

export type StatisticsApiResponse = {
  status: number,
  relics: {
    count: number,
    latestUpdate: ?string,
  },
  gemstones: {
    count: number,
    latestUpdate: ?string,
  },
}

export async function fetchApiStatus(abortController: AbortController): Promise<StatusApiResponse> {
  try {
    const response = await fetch(`${host}/api/status`, {signal: abortController.signal});
    return {
      ...await response.json(),
      status: response.status,
    };
  } catch (error) {
    return {
      health: 'down',
      status: null,
    }
  }
}

export async function fetchGemstones(abortController: AbortController): Promise<GemApiResponse> {
  try {
    const response = await fetch(`${host}/api/gemstones`, {signal: abortController.signal});
    const data = {
      ...await response.json(),
      status: response.status,
    };

    // TODO: Remove concatination
    data.gemstones = data.gemstones.concat(builtinGemstones);

    return data;
  } catch (error) {
    return {
      gemstones: [],
      status: null,
    }
  }


  // return {
  //   status: 200,
  //   gemstones: builtinGemstones,
  // }
}

export async function fetchTags(abortController: AbortController): Promise<TagsApiResponse> {
  try {
    const response = await fetch(`${host}/api/tags`, {signal: abortController.signal});
    return {
      ...await response.json(),
      status: response.status,
    };
  } catch (error) {
    return {
      tags: [],
      status: null,
    }
  }
}

export async function fetchStatistics(abortController: AbortController): Promise<StatisticsApiResponse> {
  try {
    const response = await fetch(`${host}/api/statistics`, {signal: abortController.signal});
    return {
      ...await response.json(),
      status: response.status,
    };
  } catch (error) {
    return {
      status: null,
    }
  }
}

// ####### Writing Endpoints ###################################################

export type PostGemstoneApiResponse = {
  id?: string,
  error?: string,
  fields?: {
    [string]: string,
  },
  status: number,
}

function getPostConfig (data: Object, abortController: AbortController): Object {
  return {
    signal: abortController.signal,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  };
}

export async function postNewGemstone(gemstone: Gemstone, abortController: AbortController): Promise<PostGemstoneApiResponse> {
  await sleep(1000);

  try {
    const response = await fetch(`${host}/api/gemstones`, getPostConfig(gemstone, abortController));
    return {
      ...await response.json(),
      status: response.status,
    };
  } catch (error) {
    return {
      id: null,
      status: null,
    }
  }
}
