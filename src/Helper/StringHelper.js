// @flow

export const filterAlphanumerics = (input: string): string => {
  return input.replace(/[^a-zA-Z0-9]*/, '');
}

export const upperCaseFirst = (input: string): string => {
  return input.charAt(0).toUpperCase() + input.slice(1)
}
