// @flow

import type {Gemstone} from "../../Types/Gemstone";

export const builtinGemstones: Array<Gemstone> = [
  {
    id: '_______1',
    name: 'Amethyst',
    color: 'purple',
    description: 'Lorem ipsum dolor sit amet',
    images: [
      {
        id: 'vkfd',
        source: 'https://upload.wikimedia.org/wikipedia/commons/1/1a/Amethyst_Druse.jpg',
        title: 'Cluster',
      },
    ],
    references: [
      {
        id: 'Asdjkdfg',
        source: 'https://de.wikipedia.org/wiki/Amethyst',
        title: 'Wikipedia',
      },
    ],
    tags: [
      {
        id: 'bit-0001',
        name: 'natural',
        creationDate: '2021-01-01',
      },
      {
        id: 'bit-0002',
        name: 'real',
        creationDate: '2021-01-01',
      },
    ],
  },
  {
    id: '_______2',
    name: 'Citric',
    color: 'yellow',
    description: 'Lorem ipsum dolor sit amet',
    images: [
      {
        id: '123123',
        source: 'https://fit-weltweit.de/wp-content/uploads/2018/08/Citrin-Bedeutung-Wirkung-Edelstein.jpg',
        title: 'Cluster',
      },
      {
        id: '123124',
        source: 'https://upload.wikimedia.org/wikipedia/commons/8/89/Quartz_47.jpg',
        title: 'Single',
      }
    ],
    references: [
      {
        id: '123dfv',
        source: 'https://de.wikipedia.org/wiki/Citrin',
        title: 'Wikipedia',
      },
    ],
    tags: [
      {
        id: 'bit-0001',
        name: 'natural',
        creationDate: '2021-01-01',
      },
      {
        id: 'bit-0002',
        name: 'real',
        creationDate: '2021-01-01',
      },
    ],
  },
];
