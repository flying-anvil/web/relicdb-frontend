// @flow
import React from 'react';
import {Table} from "react-bootstrap";
import GemstoneRow from "../Categories/Gemstone/GemstoneRow";
import type {Gemstone} from "../../Types/Gemstone";

type Props = {
  dataset: Array<Gemstone>,
  showTags: boolean,
};

export default function DbTable(props: Props) {
  const {dataset, showTags} = props;

  return (
    <Table className={'mt-4'} striped bordered hover>
      <thead>
      <tr>
        <th>Name</th>
        <th>Color</th>
        <th>Description</th>
        <th>Images</th>
        <th>References</th>
        {showTags && <th>Tags</th>}
      </tr>
      </thead>
      <tbody>
        {dataset.map((data) => <GemstoneRow data={data} showTags={showTags} key={data.name}/>)}
      </tbody>
    </Table>
  );
}
