// @flow
import React from 'react';
import type {Reference} from "../../Types/Reference";

type Props = {
  references: Array<Reference>,
};

export default function CellReferences(props: Props) {
  const {references} = props;

  return (
    <ul>
      {references.map((reference: Reference) =>
        <li key={reference.id}>
          <a href={reference.source} target={'_blank'} rel={'noreferrer'}>{reference.title}</a>
        </li>
      )}
    </ul>
  );
}
