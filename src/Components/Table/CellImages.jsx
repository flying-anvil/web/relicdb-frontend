// @flow
import React from 'react';
import type {ImageType} from "../../Types/Image";
import {Row} from "react-bootstrap";

type Props = {
  images: Array<ImageType>,
};

export default function CellImages(props: Props) {
  const {images} = props;

  return (
    <Row>
      {images.map((image: ImageType) =>
        <div className={'ml-2'} key={image.id}>
          <img className={'img-small'} src={image.source} alt={image.title} title={image.title}/>
        </div>
      )}
    </Row>
  );
}
