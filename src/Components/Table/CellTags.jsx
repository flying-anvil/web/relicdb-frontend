// @flow
import React from 'react';
import {upperCaseFirst} from "../../Helper/StringHelper";
import {Badge} from "react-bootstrap";
import type {Tag} from "../../Types/Tag";

type Props = {
  tags: Array<Tag>,
};

export default function CellTags(props: Props) {
  const {tags} = props;

  return (
    <ul>
      {tags.map((tag: Tag) =>
        <li key={tag.id}>
          <Badge variant={'info'}>{upperCaseFirst(tag.name)}</Badge>
        </li>
      )}
    </ul>
  );
}
