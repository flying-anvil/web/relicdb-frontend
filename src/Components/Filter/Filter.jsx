// @flow
import React, {useEffect, useRef, useState} from 'react';
import {Button, Card, Col, Form, Row, Spinner} from "react-bootstrap";
import {useApiTags} from "../../Hooks/useApiHooks";
import {upperCaseFirst} from "../../Helper/StringHelper";
import type {Tag} from "../../Types/Tag";
import {BiTrash, BsToggles} from "react-icons/all";

type Props = {
  onChange?: Function,
  filterTime?: number,
};

export default function Filter(props: Props) {
  const {onChange, filterTime} = props;
  const [expanded, setExpanded] = useState<boolean>(false);
  const [filter, setFilter] = useState<Object>(null);
  const timeoutRef = useRef();

  const tags = useApiTags();

  useEffect(() => {
    if (filter === null || !onChange) {
      return;
    }

    if (timeoutRef.current !== undefined) {
      clearTimeout(timeoutRef.current);
    }

    timeoutRef.current = setTimeout(() => {
      timeoutRef.current = undefined;
      onChange(filter);
    }, filterTime || 250);
  }, [onChange, filterTime, filter]);


  const updateSingleFilter = (event) => {
    const target = event.target;
    setFilter((old) => ({...old, [target.name]: target.value}));
  }

  const updateTags = (event) => {
    const select = event.target;
    const tags = [];

    for (let i = 0; i < select.length; i++) {
      const option = select.options[i];
      if(option.selected === true) {
        tags.push(option.value);
      }
    }

    setFilter((old) => ({...old, tags: tags}));
  }

  return (
    <Card>
      <Card.Header className={'cursor-pointer pt-2 pb-0'} onClick={() => setExpanded(!expanded)}>
        <Row>
          <Col>
            <Card.Title as='h5'>Filter</Card.Title>
          </Col>
        <Col className={'text-right'}>
          {/*<Button size={'sm'} variant={'danger'} onClick={(e) => {e.stopPropagation(); setFilter(null)}}>*/}
          {/*  (De-)Activate <BsToggles/>*/}
          {/*</Button>*/}
        </Col>
        </Row>
      </Card.Header>
      {expanded &&
        <Card.Body>

          <Row>
            <Col md={6}>
              <Form.Group controlId="filter-name">
                <Form.Label>Name</Form.Label>
                <Form.Control type="text" name={'name'} placeholder="Filter Name" size={'sm'} onChange={updateSingleFilter}/>
              </Form.Group>
              <Form.Group controlId="filter-color">
                <Form.Label>Color</Form.Label>
                <Form.Control type="text" name={'color'} placeholder="Filter Color" size={'sm'} onChange={updateSingleFilter}/>
              </Form.Group>
            </Col>

            <Col md={6}>
              <Form.Group controlId="filter-tag">
                <Form.Label>
                  Tags
                  {tags.length === 0 && <Spinner animation={"border"} size={'sm'} className={'ml-3'}/>}
                </Form.Label>
                <Form.Control as='select' multiple htmlSize={5} size={'sm'} onChange={updateTags}>
                  {tags.map((tag: Tag) => <option value={tag.id} key={tag.id}>{upperCaseFirst(tag.name)}</option>)}
                </Form.Control>
              </Form.Group>
            </Col>
          </Row>
        </Card.Body>
      }
    </Card>
  );
}
