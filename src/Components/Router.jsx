// @flow
import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Navigation from "./Navigation/Navigation";
import {Col} from "react-bootstrap";
import Relic from "./Categories/Relic/Relic";
import GemstoneDb from "./Categories/Gemstone/GemstoneDb";
import CategoryOverview from "./Categories/CategoryOverview";

type Props = {};

export default function Router(props: Props) {
  return (
    <BrowserRouter>
      {/* TODO: Why does Navigation not work when inside the Switch? */}
      <Navigation/>
      <Switch>
        <Col md={12}>

          <Route exact path={'/'}>
            <CategoryOverview/>
          </Route>
          <Route exact path={'/relic'}>
            <Relic/>
          </Route>
          <Route exact path={'/gemstone'}>
            <GemstoneDb/>
          </Route>

        </Col>
      </Switch>
    </BrowserRouter>
  );
};
