// @flow
import React from 'react';
import {Badge, Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import NavItem from "./NavItem";
import {useApiStatus} from "../../Hooks/useApiHooks";
import type {StatusApiResponse} from "../../Helper/ApiClient";
// import {AiFillDatabase, GiAncientSword, GiGems} from "react-icons/all";

type Props = {};

const pathToIcon = () => {
  return null;

  // switch (window.location.pathname) {
  //   case '/relic':
  //     return <GiAncientSword/>
  //   case '/gemstone':
  //     return <GiGems/>
  //   default:
  //     return <AiFillDatabase/>
  // }
}

const apiStatusToVariant = (status: StatusApiResponse) => {
  switch (status.health) {
    case 'pending':
      return 'secondary';
    case 'healthy':
      return 'success';
    case 'down':
      return 'danger';

    default:
      return 'secondary';
  }
}

export default function Navigation(props: Props) {
  const apiStatus = useApiStatus();

  return (
    <>
      <Navbar>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Link to={'/'} className="navbar-brand">Relic DB {pathToIcon()}</Link>

          <Nav className="mr-4">
            <NavItem as={Link} to={'/relic'} text={'Relic'}/>
          </Nav>
          <Nav className="mr-4">
            <NavItem as={Link} to={'/gemstone'} text={'Gemstone'}/>
          </Nav>

        </Navbar.Collapse>
        <span className={'mr-4 px-0 py-0'} style={{position: 'absolute', right: '0px'}}>
          API Status <Badge className={'no-select'} variant={apiStatusToVariant(apiStatus)}>&nbsp;&nbsp;</Badge>
        </span>
      </Navbar>
    </>
  );
};
