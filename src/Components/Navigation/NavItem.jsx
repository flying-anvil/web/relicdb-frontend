// @flow
import React from 'react';
import {Nav} from "react-bootstrap";
import {Link, useLocation} from "react-router-dom";

type Props = {
  to: string,
  text: string,
};

export default function NavItem(props: Props) {
  const {to, text} = props;
  const {pathname} = useLocation();
  const active = to === pathname;

  return (
    <Nav.Item>
      <Link to={to} className={`nav-link${active ? ' active' : ''}`}>{text}</Link>
    </Nav.Item>
  );
};
