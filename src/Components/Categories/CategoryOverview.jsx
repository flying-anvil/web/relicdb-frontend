// @flow
import React from 'react';
import {Badge, Card, Col, Row, Spinner} from "react-bootstrap";
import {useApiStatistics} from "../../Hooks/useApiHooks";
import CommonStatistics from "./Statistics/CommonStatistics";

type Props = {};

export default function CategoryOverview(props: Props) {
  const statistics = useApiStatistics();

  return (
    <Row className={'mt-4'}>
      <Col>
        <Card>
          <Card.Header as={'h5'}>
            Relics
          </Card.Header>
          <Card.Body>
            <span>They are old</span>
            <hr/>
            <CommonStatistics statistics={statistics.relics}/>
          </Card.Body>
        </Card>
      </Col>
      <Col>
        <Card>
          <Card.Header as={'h5'}>
            Gemstones
          </Card.Header>
          <Card.Body>
            <span>They are beautiful</span>
            <hr/>
            <CommonStatistics statistics={statistics.gemstones}/>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
