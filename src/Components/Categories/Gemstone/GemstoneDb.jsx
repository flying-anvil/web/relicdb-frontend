// @flow
import React, {useMemo, useState} from 'react';
import type {Gemstone} from "../../../Types/Gemstone";
import DbTable from "../../Table/DbTable";
import Filter from "../../Filter/Filter";
import {useApiGemstones} from "../../../Hooks/useApiHooks";
import {Button, Col, Row} from "react-bootstrap";
import GemstoneNewForm from "./Form/New/GemstoneNewForm";
import type {Tag} from "../../../Types/Tag";

type Props = {};

const gemstoneHasTag = (gemstone: Gemstone, tags: Array<string>): boolean => {
  if (!tags || tags.length === 0) {
    return true;
  }

  const gemstoneStringTagIds = gemstone.tags.map((tag: Tag) => tag.id);
  for (const filterTag of tags) {
    if (gemstoneStringTagIds.includes(filterTag)) {
      return true;
    }
  }

  return false;
}

const filterGemstones = (gemstones, filter) => {
  if (!filter) {
    return gemstones;
  }

  const lowerFilterName  = (filter.name  || '').toLowerCase();
  const lowerFilterColor = (filter.color || '').toLowerCase();

  return gemstones.filter((gemstone) => {
    const matchesName  = lowerFilterName  === '' ? true : gemstone.name.toLowerCase().includes(lowerFilterName);
    const matchesColor = lowerFilterColor === '' ? true : gemstone.color.toLowerCase().includes(lowerFilterColor);
    const matchesTag   = gemstoneHasTag(gemstone, filter.tags);

    if (filter.mode === 'or') {
      return matchesName || matchesColor || matchesTag;
    }

    return matchesName && matchesColor && matchesTag;
  });
}

export default function GemstoneDb(props: Props) {
  const [newIsOpen, setNewIsOpen] = useState<boolean>(false);
  const [showTags, setShowTags]   = useState<boolean>(true);
  const [filter, setFilter] = useState<Object>(null);

  const gemstones         = useApiGemstones();
  const filteredGemstones = useMemo(() => filterGemstones(gemstones, filter), [gemstones, filter]);

  return (
    <>
      <Row>
        <Col md={10}>
          <Filter onChange={setFilter} filterTime={1000}/>
        </Col>
        <Col md={2}>
          <Button variant={"success"} onClick={() => setNewIsOpen((old) => !old)}>Add</Button>
          <GemstoneNewForm isOpen={newIsOpen} onClose={() => setNewIsOpen(false)}/>
        </Col>
      </Row>
      <DbTable dataset={filteredGemstones || []} showTags={showTags}/>
    </>
  );
}
