// @flow
import React from 'react';
import type {Gemstone} from "../../../Types/Gemstone";
import {upperCaseFirst} from "../../../Helper/StringHelper";
import CellImages from "../../Table/CellImages";
import CellReferences from "../../Table/CellReferences";
import CellTags from "../../Table/CellTags";

type Props = {
  data: Gemstone,
  showTags?: boolean,
};

export default function GemstoneRow(props: Props) {
  const {data, showTags = false} = props;
  const {name, color, description, images, references, tags} = data;

  return (
    <tr>
      <td>{upperCaseFirst(name)}</td>
      <td>{upperCaseFirst(color)}</td>
      <td>{upperCaseFirst(description)}</td>
      <td><CellImages images={images}/></td>
      <td><CellReferences references={references}/></td>
      {showTags && <td><CellTags tags={tags}/></td>}
    </tr>
  );
}
