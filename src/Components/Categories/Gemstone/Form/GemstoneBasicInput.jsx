// @flow
import React from 'react';
import {Col, Form, Spinner} from "react-bootstrap";
import type {Tag} from "../../../../Types/Tag";
import {upperCaseFirst} from "../../../../Helper/StringHelper";
import {useApiTags} from "../../../../Hooks/useApiHooks";

type Props = {
  errors: ?Object,
};

export default function GemstoneBasicInput(props: Props) {
  const {errors} = props;
  const tags = useApiTags();

  return (
    <Form.Row>
      <Col md={9}>
        <Form.Row>
          <Col md={7}>
            <Form.Group controlId={'gemstone-new-name'}>
              <Form.Label>Name</Form.Label>
              <Form.Control type={'text'} name={'gemstone-new-name'}/>
              {errors && errors.fields.name && <Form.Text className={'text-danger'}>{errors.fields.name}</Form.Text>}
            </Form.Group>
          </Col>

          <Col md={5}>
            <Form.Group controlId={'gemstone-new-color'}>
              <Form.Label>Color</Form.Label>
              <Form.Control type={'text'} name={'gemstone-new-color'}/>
              {errors && errors.fields.color && <Form.Text className={'text-danger'}>{errors.fields.color}</Form.Text>}
            </Form.Group>
          </Col>
        </Form.Row>

        <Form.Group controlId={'gemstone-new-description'}>
          <Form.Label>Description</Form.Label>
          <Form.Control as={'textarea'} rows={3} name={'gemstone-new-description'}/>
          {errors && errors.fields.description && <Form.Text className={'text-danger'}>{errors.fields.description}</Form.Text>}
        </Form.Group>
      </Col>

      <Col md={3}>
        <Form.Group controlId="gemstone-new-tags" className={'pl-4'}>
          <Form.Label>
            Tags
            {tags.length === 0 && <Spinner animation={"border"} size={'sm'} className={'ml-3'}/>}
          </Form.Label>
          <Form.Control as='select' multiple htmlSize={8} size={'sm'}>
            {tags.map((tag: Tag) => <option value={tag.id} key={tag.id}>{upperCaseFirst(tag.name)}</option>)}
          </Form.Control>
        </Form.Group>
      </Col>
    </Form.Row>
  );
}
