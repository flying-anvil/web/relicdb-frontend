// @flow
import React, {useCallback, useRef, useState} from 'react';
import {Button, Form, Modal, Spinner} from "react-bootstrap";
import type {ImageType} from "../../../../../Types/Image";
import GemstoneBasicInput from "../GemstoneBasicInput";
import GemstoneImagesInput from "../GemstoneImagesInput";
import GemstoneReferencesInput from "../GemstoneReferencesInput";
import {PostGemstoneApiResponse, postNewGemstone} from "../../../../../Helper/ApiClient";

type Props = {
  isOpen: boolean,
  onClose: Function,
};

const createDummyId = (): ImageType => {
  return {
    id: Math.floor(Math.random() * 0xFFFF).toString(16).toUpperCase(),
  };
}

export default function GemstoneNewForm(props: Props) {
  const {isOpen, onClose} = props;
  const [images, setImages] = useState<Array<string>>([createDummyId()]);
  const [references, setReferences] = useState<Array<string>>([createDummyId()]);
  const [apiPending, setApiPending] = useState<boolean>(false);
  const [errors, setErrors] = useState<Object>(null);
  const formRef = useRef();

  const addImage = useCallback(() => {
    setImages((old) => [...old, createDummyId()]);
  }, []);

  const removeImage = useCallback((id: string) => {
    setImages((old) => old.filter((image) => image.id !== id));
  }, []);

  const addReference = useCallback(() => {
    setReferences((old) => [...old, createDummyId()]);
  }, []);

  const removeReference = useCallback((id: string) => {
    setReferences((old) => old.filter((reference) => reference.id !== id));
  }, []);

  const onSubmit = useCallback(() => {
    if (apiPending) {
      return;
    }

    const form = formRef.current;

    const name        = form.elements['gemstone-new-name'].value;
    const color       = form.elements['gemstone-new-color'].value;
    const description = form.elements['gemstone-new-description'].value;
    const tags        = form.elements['gemstone-new-tags'];

    const formImages     = [];
    const formReferences = [];
    const formTags       = [];

    for (const image of images) {
      const source = form.elements[`gemstone-new-image-source-${image.id}`].value;
      const title  = form.elements[`gemstone-new-image-title-${image.id}`].value;

      if (source !== '') {
        formImages.push({source, title});
      }
    }

    for (const reference of references) {
      const source = form.elements[`gemstone-new-reference-source-${reference.id}`].value;
      const title  = form.elements[`gemstone-new-reference-text-${reference.id}`].value;

      if (source !== '') {
        formReferences.push({source, title});
      }
    }

    for (let i = 0; i < tags.length; i++) {
      const option = tags.options[i];
      if(option.selected === true) {
        formTags.push(option.value);
      }
    }

    const apiBody = {
      name: name,
      color: color,
      description: description,
      images: formImages,
      references: formReferences,
      tags: formTags,
    }

    console.log(apiBody);
    setApiPending(true);
    const abort = new AbortController();
    postNewGemstone(apiBody, abort).then((response: PostGemstoneApiResponse) => {
      setApiPending(false);
      console.log(response);

      // TODO: Do something when 200, refresh table

      if (response.status >= 400 && response.status < 500) {
        setErrors({
          message: response.error,
          fields: response.fields,
        })
      }
    });
  }, [images, references, apiPending]);

  return (
    <Modal
      size='xl'
      // dialogClassName="modal-xxl"
      show={isOpen}
      onHide={() => {setErrors(null); onClose()}}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          Add new Gemstone
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form ref={formRef} onSubmit={onSubmit}>
          <GemstoneBasicInput errors={errors}/>
          <hr/>

          <h5 className={'mb-3'}>Images</h5>
          <GemstoneImagesInput images={images} addImage={addImage} removeImage={removeImage}/>
          <hr/>

          <h5 className={'mb-3'}>References</h5>
          <GemstoneReferencesInput references={references} addReference={addReference} removeReference={removeReference}/>

        </Form>
      </Modal.Body>
      <Modal.Footer>
        {errors && <span className={'text-danger'}>{errors.message}</span>}
        <Button variant={apiPending ? 'outline-success' : 'success'} onClick={onSubmit} disabled={apiPending}>
          {apiPending ? 'Creating' : 'Create'}
          {apiPending && <Spinner animation={'border'} size={'sm'} className={'ml-2'}/>}
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
