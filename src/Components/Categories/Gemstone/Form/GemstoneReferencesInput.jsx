// @flow
import React from 'react';
import type {Reference} from "../../../../Types/Reference";
import {Button, Col, Form} from "react-bootstrap";
import {BiTrash} from "react-icons/all";

type Props = {
  references: Reference,
  addReference: Function,
  removeReference: Function,
};

export default function GemstoneReferencesInput(props: Props) {
  const {references, addReference, removeReference} = props;

  return (
    <>
      {references.map((reference: Reference, index: number) =>
        <Form.Row key={reference.id} className={'hover-dark'}>
          <Col md={6}>
            <Form.Group controlId={`gemstone-new-reference-source-${reference.id}`} className={'mb-0 mt-1'}>
              {index === 0 && <Form.Label>Source</Form.Label>}
              <Form.Control type={'text'} size={'sm'} reference-id={reference.id} name={`gemstone-new-reference-source-${reference.id}`}/>
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group controlId={`gemstone-new-reference-text-${reference.id}`} className={'mb-0 mt-1'}>
              {index === 0 && <Form.Label>Text</Form.Label>}
              <Form.Control type={'text'} size={'sm'} reference-id={reference.id} name={`gemstone-new-reference-text-${reference.id}`}/>
            </Form.Group>
          </Col>
          <Col md={1}/>
          <Col md={1}>
            <Form.Group className={'mb-1 mt-1'}>
              {index === 0 && <Form.Label>&nbsp;</Form.Label>}
              <Form.Control as={Button} size={'sm'} variant={'danger'} onClick={() => removeReference(reference.id)}><BiTrash/></Form.Control>
            </Form.Group>
          </Col>
        </Form.Row>
      )}

      <Button variant={'success'} size={"sm"} className={'mt-2'} onClick={addReference}>Add Reference</Button>

    </>
  );
}
