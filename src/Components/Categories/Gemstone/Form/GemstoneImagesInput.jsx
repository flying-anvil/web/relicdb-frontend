// @flow
import React from 'react';
import type {ImageType} from "../../../../Types/Image";
import {Button, Col, Form} from "react-bootstrap";
import {BiTrash} from "react-icons/all";

type Props = {
  images: ImageType,
  addImage: Function,
  removeImage: Function,
};

export default function GemstoneImagesInput(props: Props) {
  const {images, addImage, removeImage} = props;

  return (
    <>
      {images.map((image: ImageType, index: number) =>
        <Form.Row key={image.id} className={'hover-dark'}>
          <Col md={6}>
            <Form.Group controlId={`gemstone-new-image-source-${image.id}`} className={'mb-0 mt-1'}>
              {index === 0 && <Form.Label>Source</Form.Label>}
              <Form.Control type={'text'} size={'sm'} name={`gemstone-new-image-source-${image.id}`}/>
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group controlId={`gemstone-new-image-title-${image.id}`} className={'mb-0 mt-1'}>
              {index === 0 && <Form.Label>Title</Form.Label>}
              <Form.Control type={'text'} size={'sm'} name={`gemstone-new-image-title-${image.id}`}/>
            </Form.Group>
          </Col>
          <Col md={1}/>
          <Col md={1}>
            <Form.Group className={'mb-1 mt-1'}>
              {index === 0 && <Form.Label>&nbsp;</Form.Label>}
              <Form.Control as={Button} size={'sm'} variant={'danger'} onClick={() => removeImage(image.id)}><BiTrash/></Form.Control>
            </Form.Group>
          </Col>
        </Form.Row>
      )}

      <Button variant={'success'} size={"sm"} className={'mt-2'} onClick={addImage}>Add Image</Button>

    </>
  );
}
