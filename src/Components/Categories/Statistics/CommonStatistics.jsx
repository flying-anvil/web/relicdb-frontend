// @flow
import React from 'react';
import type {Statistics} from "../../../Types/Statistics";
import {Badge, Spinner} from "react-bootstrap";

type Props = {
  statistics: Statistics,
};

export default function CommonStatistics(props: Props) {
  const {statistics} = props;
  const hasStatistics = statistics !== null;

  if (!hasStatistics) {
    return <div>Fetching statistics… <Spinner animation={'border'} size={'sm'}/></div>;
  }

  const {count, latestUpdate} = statistics;

  return (
    <>
      <div>Count: <Badge variant={'primary'}>{count}</Badge></div>
      <div>Latest Update: <Badge variant={'primary'}>
        {latestUpdate !== null ? latestUpdate.toLocaleString() : 'never'}
      </Badge></div>
    </>
  );
}
