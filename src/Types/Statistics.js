// @flow

export type Statistics = {
  count: number,
  latestUpdate: Date,
};

export type AllStatistics = {
  relics: Statistics,
  gemstones: Statistics,
}
