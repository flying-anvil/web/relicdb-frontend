// @flow

export type Image = {
  id?: string,
  source: string,
  title: string,
}

export type ImageType = Image;
