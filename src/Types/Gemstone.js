// @flow

import type {Reference} from "./Reference";
import type {Image} from "./Image";
import type {Tag} from "./Tag";

export type Gemstone = {
  name: string,
  color: string,
  description: string,
  images: Array<Image>,
  references: Array<Reference>,
  tags: Array<Tag>,
}
