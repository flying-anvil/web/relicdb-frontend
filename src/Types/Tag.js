// @flow

export type Tag = {
  id: string,
  name: string,
  creationDate: string,
}
