// @flow

export type Reference = {
  id?: string,
  source: string,
  title: string,
}
