// @flow

import {useEffect, useState} from "react";
import {fetchApiStatus, fetchGemstones, fetchStatistics, fetchTags} from "../Helper/ApiClient";
import type {Gemstone} from "../Types/Gemstone";
import type {StatusApiResponse} from "../Helper/ApiClient";
import type {AllStatistics} from "../Types/Statistics";
import type {Tag} from "../Types/Tag";

export function useApiStatus(): StatusApiResponse {
  const [status, setStatus] = useState<StatusApiResponse>({health: 'pending'});

  useEffect(() => {
    const abortController = new AbortController();

    (async () => {
      const response = await fetchApiStatus(abortController);
      setStatus(response);
    })();

    return () => {
      abortController.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return status;
}

export function useApiGemstones(): Array<Gemstone> {
  const [gemstones, setGemstones] = useState<Array<Gemstone>>([]);

  useEffect(() => {
    const abortController = new AbortController();

    (async () => {
      const response = await fetchGemstones(abortController);

      if (response.status === 200) {
        setGemstones(response.gemstones);
      }
    })();

    return () => {
      abortController.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return gemstones;
}

export function useApiTags(): Array<Tag> {
  const [tags, setTags] = useState<Array<Tag>>([]);

  useEffect(() => {
    const abortController = new AbortController();

    (async () => {
      const response = await fetchTags(abortController);

      if (response.status === 200) {
        setTags(response.tags);
      }
    })();

    return () => {
      abortController.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return tags;
}

export function useApiStatistics(): AllStatistics {
  const [statistics, setStatistics] = useState<AllStatistics>({relics: null, gemstones: null});

  useEffect(() => {
    const abortController = new AbortController();

    (async () => {
      const response = await fetchStatistics(abortController);

      if (response.status === 200) {
        setStatistics({
          relics: {
            count: response.relics.count,
            latestUpdate: response.relics.latestUpdate ? new Date(response.relics.latestUpdate) : null,
          },
          gemstones: {
            count: response.gemstones.count,
            latestUpdate: response.gemstones.latestUpdate ? new Date(response.gemstones.latestUpdate) : null,
          },
        });
      }
    })();

    return () => {
      abortController.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return statistics;
}
